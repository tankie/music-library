import { Action } from '@ngrx/store';
import { Playlist } from 'src/app/modules/playlist/interfaces/playlist';

export enum PlaylistActionTypes {
  LoadPlaylists = '[Playlist] Load Playlists',
  PlaylistsLoadSuccess = '[Playlist] Playlists loaded - Success',
  PlaylistsLoadError = '[Playlist] Playlists loaded - Error',
  FilterPlaylist = '[Playlist] Filter playlist',
}

export class LoadPlaylists implements Action {
  readonly type = PlaylistActionTypes.LoadPlaylists;
}

export class PlaylistsLoadError implements Action {
  readonly type = PlaylistActionTypes.PlaylistsLoadError;
  constructor(public payload: any) {}
}

export class PlaylistsLoadSuccess implements Action {
  readonly type = PlaylistActionTypes.PlaylistsLoadSuccess;
  constructor(public payload: Playlist[]) {}
}

export class FilterPlaylist implements Action {
  readonly type = PlaylistActionTypes.FilterPlaylist;
  constructor(public payload: string) {}
}

export type PlaylistActions =
  | LoadPlaylists
  | PlaylistsLoadError
  | PlaylistsLoadSuccess
  | FilterPlaylist;
