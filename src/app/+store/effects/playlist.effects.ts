import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { mergeMap, map, catchError, switchMap } from 'rxjs/operators';

import { PlaylistState } from '../reducers/playlist.reducer';
import { PlaylistActionTypes, PlaylistsLoadError } from '../actions/playlist.actions';
import { PlaylistService } from 'src/app/modules/playlist/services/playlist.service';

import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class PlaylistEffects {
  @Effect()
  loadPlaylists$ = this.actions$.pipe(
    ofType(PlaylistActionTypes.LoadPlaylists),
    mergeMap(() =>
      this.playlistService.getPlaylists().pipe(
        map((playlists) => ({
          type: PlaylistActionTypes.PlaylistsLoadSuccess,
          payload: playlists,
        })),
        catchError((error) => {
          this.store.dispatch(new PlaylistsLoadError(error));
          return EMPTY;
        })
      )
    )
  );

  @Effect()
  loadPlaylistsError$ = this.actions$.pipe(
    ofType(PlaylistActionTypes.PlaylistsLoadError),
    switchMap((action: PlaylistsLoadError) => {
      this.snackBar.open(action.payload.message, 'OK', { duration: 5000 });
      return EMPTY;
    })
  );

  constructor(
    private actions$: Actions,
    private playlistService: PlaylistService,
    private snackBar: MatSnackBar,
    private store: Store<PlaylistState>
  ) {}
}
