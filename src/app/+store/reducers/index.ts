import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../../environments/environment';
import * as fromPlaylist from './playlist.reducer';

export interface State {
  playlist: fromPlaylist.PlaylistState;
}

export const reducers: ActionReducerMap<State> = {
  playlist: fromPlaylist.reducer,
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
