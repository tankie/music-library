import { reducer } from './playlist.reducer';
import * as fromPlaylist from './playlist.reducer';
import * as fromActions from '../actions/playlist.actions';
import { mockPlaylistData } from 'src/app/utils/playlists.mock';

describe('Playlist Reducer', () => {
  const { initialState } = fromPlaylist;

  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });

  describe('FilterPlaylist action', () => {
    it('should set a filter', () => {
      const action = new fromActions.FilterPlaylist('asd');
      const state = fromPlaylist.reducer(initialState, action);

      expect(state.filter).toEqual('asd');
    });
  });

  describe('PlaylistsLoadSuccess action', () => {
    it('should populate the playlists', () => {
      const action = new fromActions.PlaylistsLoadSuccess(
        mockPlaylistData.featuredPlaylists.content
      );
      const state = fromPlaylist.reducer(initialState, action);

      expect(state.playlists).toEqual(mockPlaylistData.featuredPlaylists.content);
    });
  });
});
