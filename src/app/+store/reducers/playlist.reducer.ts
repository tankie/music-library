import { Playlist } from 'src/app/modules/playlist/interfaces/playlist';
import { PlaylistActionTypes, PlaylistActions } from '../actions/playlist.actions';

export interface PlaylistState {
  loaded: boolean;
  playlists: Playlist[];
  filter: string;
}

export const initialState: PlaylistState = {
  loaded: false,
  playlists: [],
  filter: '',
};

export function reducer(state = initialState, action: PlaylistActions): PlaylistState {
  switch (action.type) {
    case PlaylistActionTypes.FilterPlaylist:
      return {
        ...state,
        filter: action.payload,
      };
    case PlaylistActionTypes.PlaylistsLoadSuccess:
      return { ...state, playlists: action.payload, loaded: true };
    default:
      return state;
  }
}
