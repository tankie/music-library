import { createSelector, createFeatureSelector } from '@ngrx/store';
import { PlaylistState } from '../reducers/playlist.reducer';

const getPlaylistState = createFeatureSelector<PlaylistState>('playlist');

export const getLoaded = createSelector(
  getPlaylistState,
  (state: PlaylistState) => state.loaded
);

export const getPlaylists = createSelector(
  getPlaylistState,
  (state: PlaylistState) => state.playlists
);

export const getFilteredPlaylists = createSelector(
  getPlaylistState,
  (state: PlaylistState) => {
    if (!state.filter) {
      return state.playlists;
    }

    return state.playlists.filter(
      (p) =>
        p.name.toLowerCase().indexOf(state.filter.toLowerCase()) !== -1 ||
        p.curator_name.toLowerCase().indexOf(state.filter.toLowerCase()) !== -1
    );
  }
);
