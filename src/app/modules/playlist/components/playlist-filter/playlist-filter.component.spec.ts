import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA, DebugElement } from '@angular/core';
import { Store, StoreModule } from '@ngrx/store';

import { PlaylistFilterComponent } from './playlist-filter.component';
import { TestingModule, MockStore } from 'src/app/utils/testing.module';
import { PlaylistState } from 'src/app/+store/reducers/playlist.reducer';

import * as fromRoot from '../../../../+store/reducers';
import * as fromPlaylist from '../../../../+store/reducers/playlist.reducer';
import { getFilteredPlaylists } from 'src/app/+store/selectors/playlist.selectors';
import { mockPlaylistData } from 'src/app/utils/playlists.mock';
import { PlaylistsLoadSuccess } from 'src/app/+store/actions/playlist.actions';

describe('PlaylistFilterComponent', () => {
  let component: PlaylistFilterComponent;
  let fixture: ComponentFixture<PlaylistFilterComponent>;
  let store: MockStore<PlaylistState>;
  const searchString = 'week';
  let input: DebugElement;
  let dispatchSpy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlaylistFilterComponent],
      imports: [
        TestingModule,
        StoreModule.forRoot({
          ...fromRoot.reducers,
          feature: fromPlaylist.reducer,
        }),
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    store = TestBed.get(Store);
    dispatchSpy = spyOn(store, 'dispatch').and.callThrough();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update the search value when user types', fakeAsync(() => {
    input = fixture.debugElement.query(By.css('input'));
    expect(component.value).toBeFalsy();

    input.nativeElement.value = searchString;
    input.nativeElement.dispatchEvent(new Event('input'));
    tick();

    expect(component.value).toBe(searchString);
  }));

  it('should dispatch an action when user types', fakeAsync(() => {
    input = fixture.debugElement.query(By.css('input'));

    input.nativeElement.dispatchEvent(new Event('keyup'));
    tick();

    expect(dispatchSpy).toHaveBeenCalled();
  }));

  it('should return all playlists if the filter is not set', fakeAsync(() => {
    const playlists = mockPlaylistData.featuredPlaylists.content;
    store.dispatch(new PlaylistsLoadSuccess(playlists));

    store.select(getFilteredPlaylists).subscribe((filteredPlaylists) => {
      expect(filteredPlaylists).toBe(playlists);
    });
  }));

  it('should filter the array of playlists from the store', fakeAsync(() => {
    const playlists = mockPlaylistData.featuredPlaylists.content;
    store.dispatch(new PlaylistsLoadSuccess(playlists));

    input = fixture.debugElement.query(By.css('input'));
    input.nativeElement.value = searchString;
    input.nativeElement.dispatchEvent(new Event('input'));
    input.nativeElement.dispatchEvent(new Event('keyup'));
    tick();

    store.select(getFilteredPlaylists).subscribe((filteredPlaylists) => {
      expect(filteredPlaylists.length).toBe(1);
    });
  }));
});
