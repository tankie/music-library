import { Component, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { PlaylistState } from 'src/app/+store/reducers/playlist.reducer';
import { FilterPlaylist } from 'src/app/+store/actions/playlist.actions';

@Component({
  selector: 'app-playlist-filter',
  templateUrl: './playlist-filter.component.html',
  styleUrls: ['./playlist-filter.component.scss'],
})
export class PlaylistFilterComponent implements OnDestroy {
  value: string;

  constructor(private store: Store<PlaylistState>) {}

  applyFilter(filter): void {
    this.store.dispatch(new FilterPlaylist(filter));
  }

  ngOnDestroy(): void {
    // lazily not adding a separate ClearPlaylistFilter action
    this.store.dispatch(new FilterPlaylist(''));
  }
}
