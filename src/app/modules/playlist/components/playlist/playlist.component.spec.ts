import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Store, StoreModule } from '@ngrx/store';

import { TestingModule, MockStore } from 'src/app/utils/testing.module';
import { PlaylistState } from 'src/app/+store/reducers/playlist.reducer';
import { mockPlaylistData } from 'src/app/utils/playlists.mock';

import { PlaylistComponent } from './playlist.component';

import * as fromRoot from '../../../../+store/reducers';
import * as fromPlaylist from '../../../../+store/reducers/playlist.reducer';
import { PlaylistsLoadSuccess } from 'src/app/+store/actions/playlist.actions';

describe('PlaylistComponent', () => {
  let component: PlaylistComponent;
  let fixture: ComponentFixture<PlaylistComponent>;
  let store: MockStore<PlaylistState>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlaylistComponent],
      imports: [
        TestingModule,
        StoreModule.forRoot({
          ...fromRoot.reducers,
          feature: fromPlaylist.reducer,
        }),
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    store = TestBed.get(Store);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load a list of playlists', () => {
    const playlists = mockPlaylistData.featuredPlaylists.content;
    store.dispatch(new PlaylistsLoadSuccess(playlists));

    store.subscribe(() => {
      fixture.detectChanges();
      expect(component.playlists.length).toBe(playlists.length);
    });
  });
});
