import {
  Component,
  ViewChild,
  AfterViewInit,
  OnDestroy,
  ChangeDetectionStrategy,
} from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Playlist } from '../../interfaces/playlist';
import { Store, select } from '@ngrx/store';
import { PlaylistState } from 'src/app/+store/reducers/playlist.reducer';
import { getFilteredPlaylists } from 'src/app/+store/selectors/playlist.selectors';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlaylistComponent implements AfterViewInit, OnDestroy {
  playlists: Playlist[];
  dataSource: MatTableDataSource<Playlist>;
  connectedSource: Observable<any>;
  playlistSubscription: Subscription;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private playlistStore: Store<PlaylistState>) {}

  ngAfterViewInit() {
    this.playlistSubscription = this.playlistStore
      .pipe(select(getFilteredPlaylists))
      .subscribe((playlists) => {
        this.playlists = playlists;
        this.buildDataSource();
      });
  }

  /**
   * Helper to contain the construction of the Material `MatTableDataSource`
   * we are using in our template for "free" pagination (yay).
   */
  private buildDataSource(): void {
    this.dataSource = new MatTableDataSource(this.playlists);
    this.dataSource.paginator = this.paginator;
    this.connectedSource = this.dataSource.connect();
  }

  ngOnDestroy() {
    this.playlistSubscription.unsubscribe();
  }
}
