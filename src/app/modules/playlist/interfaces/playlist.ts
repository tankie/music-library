export interface Playlist {
  id: string;
  kind: string;
  name: string;
  url: string;
  curator_name: string;
  artwork: string;
}

export interface PlaylistPayload {
  featuredPlaylists: {
    name: string;
    content: Playlist[];
  };
}
