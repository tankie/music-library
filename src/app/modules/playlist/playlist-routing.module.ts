import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlaylistComponent } from './components/playlist/playlist.component';
import { PlaylistGuard } from './shared/playlist.guard';

const routes: Routes = [{ path: '', component: PlaylistComponent, canActivate: [PlaylistGuard] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlaylistRoutingModule {}
