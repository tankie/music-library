import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { MaterialModule } from 'src/app/shared/material.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { PlaylistRoutingModule } from './playlist-routing.module';

import { PlaylistService } from './services/playlist.service';
import * as fromPlaylist from '../../+store/reducers/playlist.reducer';
import { PlaylistEffects } from '../../+store/effects/playlist.effects';

import { PlaylistComponent } from './components/playlist/playlist.component';
import { PlaylistFilterComponent } from './components/playlist-filter/playlist-filter.component';

@NgModule({
  declarations: [PlaylistComponent, PlaylistFilterComponent],
  imports: [
    HttpClientModule,
    CommonModule,
    FormsModule,
    PlaylistRoutingModule,
    MaterialModule,
    StoreModule.forFeature('playlist', fromPlaylist.reducer),
    EffectsModule.forFeature([PlaylistEffects]),
  ],
  providers: [PlaylistService],
})
export class PlaylistModule {}
