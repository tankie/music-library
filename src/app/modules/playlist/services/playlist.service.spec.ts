import { TestBed } from '@angular/core/testing';

import { PlaylistService } from './playlist.service';
import { TestingModule } from 'src/app/utils/testing.module';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { mockPlaylistData } from 'src/app/utils/playlists.mock';
import { StoreModule } from '@ngrx/store';

import * as fromRoot from '../../../+store/reducers';
import * as fromPlaylist from '../../../+store/reducers/playlist.reducer';

class MockPlaylistService {
  apiUrl =
    'https://portal.organicfruitapps.com/programming-guides/v2/us_en-us/featured-playlists.json';

  getPlaylists() {
    return mockPlaylistData;
  }
}

describe('PlaylistService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        TestingModule,
        StoreModule.forRoot({
          ...fromRoot.reducers,
          feature: fromPlaylist.reducer,
        }),
      ],
      providers: [{ provide: PlaylistService, useClass: MockPlaylistService }],
      schemas: [NO_ERRORS_SCHEMA],
    });
  });

  it('should be created', () => {
    const service: PlaylistService = TestBed.get(PlaylistService);
    expect(service).toBeTruthy();
  });
});
