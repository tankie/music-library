import { Injectable } from '@angular/core';
import { Playlist, PlaylistPayload } from '../interfaces/playlist';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, filter } from 'rxjs/operators';

@Injectable()
export class PlaylistService {
  apiUrl =
    'https://portal.organicfruitapps.com/programming-guides/v2/us_en-us/featured-playlists.json';

  constructor(private http: HttpClient) {}

  getPlaylists(): Observable<Playlist[]> {
    return this.http.get<PlaylistPayload>(this.apiUrl).pipe(
      filter((payload) => {
        return !!payload && !!payload.featuredPlaylists;
      }),
      map((payload) => {
        // For demonstration purposes
        console.log('loading from API');
        return payload.featuredPlaylists.content;
      })
    );
  }
}
