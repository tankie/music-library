import { TestBed, async } from '@angular/core/testing';
import { Store, StoreModule } from '@ngrx/store';
import { cold } from 'jasmine-marbles';
import { PlaylistGuard } from './playlist.guard';
import { TestingModule, MockStore } from 'src/app/utils/testing.module';
import { PlaylistState } from 'src/app/+store/reducers/playlist.reducer';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import * as fromRoot from '../../../+store/reducers';
import * as fromPlaylist from '../../../+store/reducers/playlist.reducer';

describe('PlaylistGuard', () => {
  let guard: PlaylistGuard;
  let store: MockStore<PlaylistState>;
  let dispatchSpy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        TestingModule,
        StoreModule.forRoot({
          ...fromRoot.reducers,
          feature: fromPlaylist.reducer,
        }),
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [PlaylistGuard],
    }).compileComponents();

    store = TestBed.get(Store);
    dispatchSpy = spyOn(store, 'dispatch').and.callThrough();

    guard = TestBed.get(PlaylistGuard);
  }));

  it('should return an observable of `true`', () => {
    const expected = cold('(a|)', { a: true });
    expect(guard.canActivate()).toBeObservable(expected);
  });

  it('should call the `getFromStoreOrAPI` method', () => {
    spyOn(guard, 'getFromStoreOrAPI').and.callThrough();
    guard.canActivate();
    expect(guard.getFromStoreOrAPI).toHaveBeenCalledWith();
  });

  it('should dispatch an action when the Store is not populated', () => {
    guard.canActivate().subscribe(() => {
      expect(dispatchSpy).toHaveBeenCalled();
    });
  });

  /* 
    we could add tests to verify if action doesn't get called when store already populated
  */
});
