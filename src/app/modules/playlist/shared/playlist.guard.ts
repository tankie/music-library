import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Store, select } from '@ngrx/store';

import { Observable, of, EMPTY } from 'rxjs';
import { tap, take, catchError, map, switchMap } from 'rxjs/operators';

import { PlaylistState } from 'src/app/+store/reducers/playlist.reducer';
import { getPlaylists } from 'src/app/+store/selectors/playlist.selectors';
import { LoadPlaylists } from 'src/app/+store/actions/playlist.actions';
import { Playlist } from '../interfaces/playlist';

@Injectable()
export class PlaylistGuard implements CanActivate {
  constructor(private store: Store<PlaylistState>) {}

  /**
   * Helper to delegate the check/population of the store with playlist-related data.
   * This way, we can ensure the `playlists` route and all its (imaginary) children will
   * have data available in the store for consumption. (eg. a "Playlist details" screen,
   * a "Genres" screen, and so on).
   */
  getFromStoreOrAPI(): Observable<Playlist[]> {
    return this.store.pipe(
      select(getPlaylists),
      tap((playlists: Playlist[]) => {
        if (!playlists.length) {
          this.store.dispatch(new LoadPlaylists());
        } else {
          // For demonstration purposes
          console.log('loading from Store');
        }
      }),
      take(1)
    );
  }

  canActivate(): Observable<boolean> {
    return this.getFromStoreOrAPI().pipe(
      catchError(() => of(false)),
      switchMap(() => of(true))
    );
  }
}
