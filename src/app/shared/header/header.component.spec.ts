import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Store } from '@ngrx/store';

import { TestingModule, MockStore } from '../../utils/testing.module';
import { mockPlaylistData } from 'src/app/utils/playlists.mock';
import { PlaylistState, initialState } from 'src/app/+store/reducers/playlist.reducer';

import { HeaderComponent } from './header.component';

const dummyState = {
  ...initialState,
  playlists: mockPlaylistData.featuredPlaylists.content,
};

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let store: MockStore<PlaylistState>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent],
      imports: [TestingModule],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    store = TestBed.get(Store);
    store.setState({ ...dummyState } as PlaylistState);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
