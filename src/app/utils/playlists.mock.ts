export const mockPlaylistData = {
  featuredPlaylists: {
    name: 'Featured Playlists',
    content: [
      {
        id: 'pl.2b0e6e332fdf4b7a91164da3162127b5',
        kind: 'playlist',
        name: 'Best of the Week',
        url:
          'https://music.apple.com/us/playlist/best-of-the-week/pl.2b0e6e332fdf4b7a91164da3162127b5?app=music\u0026at=1000l4QJ\u0026ct=330\u0026itscg=10000\u0026itsct=330',
        curator_name: 'Apple Music Pop',
        artwork:
          'https://is5-ssl.mzstatic.com/image/thumb/Features127/v4/7d/5d/f5/7d5df5a7-25ff-4c3c-0b8f-46a1a4cdf44d/source/600x600cc.jpg',
      },
      {
        id: 'pl.f4d106fed2bd41149aaacabb233eb5eb',
        kind: 'playlist',
        name: "Today's Hits",
        url:
          'https://music.apple.com/us/playlist/todays-hits/pl.f4d106fed2bd41149aaacabb233eb5eb?app=music\u0026at=1000l4QJ\u0026ct=330\u0026itscg=10000\u0026itsct=330',
        curator_name: 'Apple Music Pop',
        artwork:
          'https://is1-ssl.mzstatic.com/image/thumb/Features117/v4/94/17/2e/94172e16-e5d7-2d2b-66ec-e72473988168/source/600x600cc.jpg',
      },
      {
        id: 'pl.abe8ba42278f4ef490e3a9fc5ec8e8c5',
        kind: 'playlist',
        name: 'The A-List: Hip-Hop',
        url:
          'https://music.apple.com/us/playlist/the-a-list-hip-hop/pl.abe8ba42278f4ef490e3a9fc5ec8e8c5?app=music\u0026at=1000l4QJ\u0026ct=330\u0026itscg=10000\u0026itsct=330',
        curator_name: 'Apple Music Hip-Hop',
        artwork:
          'https://is3-ssl.mzstatic.com/image/thumb/Features128/v4/be/ca/be/becabe5c-150a-3dd7-a0d6-a39edc1760a2/source/600x600cc.jpg',
      },
      {
        id: 'pl.5cb9c0f3ca9d4fc1bccbaf67ca6201e7',
        kind: 'playlist',
        name: 'Up Next',
        url:
          'https://music.apple.com/us/playlist/up-next/pl.5cb9c0f3ca9d4fc1bccbaf67ca6201e7?app=music\u0026at=1000l4QJ\u0026ct=330\u0026itscg=10000\u0026itsct=330',
        curator_name: 'Apple Music',
        artwork:
          'https://is3-ssl.mzstatic.com/image/thumb/Features123/v4/02/d1/c8/02d1c8b7-a769-414e-5656-de2f7fa495fd/source/600x600cc.jpg',
      },
    ],
  },
};
