import { NgModule, Injectable } from '@angular/core';
import { Store, StateObservable, ActionsSubject, ReducerManager, StoreModule } from '@ngrx/store';
import { BehaviorSubject } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './../shared/material.module';
import { FormsModule } from '@angular/forms';

@Injectable()
export class MockStore<T> extends Store<T> {
  private stateSubject = new BehaviorSubject<T>({} as T);

  constructor(
    state$: StateObservable,
    actionsObserver: ActionsSubject,
    reducerManager: ReducerManager
  ) {
    super(state$, actionsObserver, reducerManager);
    this.source = this.stateSubject.asObservable();
  }

  setState(nextState: T) {
    this.stateSubject.next(nextState);
  }
}

@NgModule({
  imports: [
    RouterTestingModule,
    HttpClientTestingModule,
    FormsModule,
    MaterialModule,
    StoreModule.forRoot({}),
    BrowserAnimationsModule,
  ],
  exports: [
    RouterTestingModule,
    HttpClientTestingModule,
    FormsModule,
    MaterialModule,
    StoreModule,
    BrowserAnimationsModule,
  ],
  providers: [{ provide: Store, useClass: MockStore }],
})
export class TestingModule {
  constructor() {}
}
